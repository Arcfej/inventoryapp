package com.example.android.inventoryapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.inventoryapp.data.InventoryContract.ProductEntry;

public class MainActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        InventoryCursorAdapter.OnProductClickListener,
        InventoryCursorAdapter.OnSalesButtonClickListener {

    private static final int INVENTORY_LOADER = 23;
    private static final int REQUEST_READ_STORAGE = 1;

    InventoryCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Ask for permission to read pictures from the storage
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_READ_STORAGE);
        }

        ListView productList = findViewById(R.id.rv_product_list);
        cursorAdapter = new InventoryCursorAdapter(this, null,
                this, this);
        productList.setAdapter(cursorAdapter);
        productList.setEmptyView(findViewById(R.id.tv_empty_list_welcome_message));


        getLoaderManager().initLoader(INVENTORY_LOADER, null, this);
    }

    public void addNewProduct() {
        startActivity(new Intent(this, EditorActivity.class));
    }

    @Override
    public void onButtonClick(long id) {
        Cursor cursor = getContentResolver().query(
                ContentUris.withAppendedId(ProductEntry.CONTENT_URI, id),
                new String[]{ProductEntry.COLUMN_QUANTITY},
                null,
                null,
                null);
        if (cursor == null) {
            return;
        }
        cursor.moveToFirst();
        int currentQuantity = cursor.getInt(cursor.getColumnIndex(ProductEntry.COLUMN_QUANTITY));
        if (currentQuantity > 0) {
            currentQuantity -= 1;
            ContentValues updatedProduct = new ContentValues();
            updatedProduct.put(ProductEntry.COLUMN_QUANTITY, currentQuantity);
            getContentResolver().update(ContentUris.withAppendedId(ProductEntry.CONTENT_URI, id),
                    updatedProduct,
                    null,
                    null);
        }
        cursor.close();
    }

    @Override
    public void onProductClick(long id, String productName) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(getString(R.string.intent_extra_key_product_id), id);
        intent.putExtra(getString(R.string.intent_extra_key_product_name), productName);
        startActivity(intent);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        // name, quantityView, price
        String[] projection = new String[]{
                ProductEntry._ID,
                ProductEntry.COLUMN_PICTURE,
                ProductEntry.COLUMN_NAME,
                ProductEntry.COLUMN_QUANTITY,
                ProductEntry.COLUMN_PRICE
        };
        return new CursorLoader(this,
                ProductEntry.CONTENT_URI,
                projection,
                null,
                null,
                ProductEntry.COLUMN_NAME + " ASC");
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor data) {
        if (data != null) {
            cursorAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        cursorAdapter.swapCursor(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_delete_inventory) {
            showDeleteAlertDialog();
            return true;
        }
        if (itemId == R.id.action_add_product) {
            addNewProduct();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteAlertDialog() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        deleteProducts();
                        dialog.dismiss();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.delete_all_dialog_message));
        builder.setPositiveButton(R.string.delete_dialog_delete_putton, listener);
        builder.setNegativeButton(R.string.delete_dialog_cancel_button, listener);
        builder.show();
    }

    private void deleteProducts() {
        int success = getContentResolver().delete(
                ProductEntry.CONTENT_URI,
                null,
                null);
        if (success > 0) {
            Toast.makeText(this, R.string.delete_single_product_success_message,
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.error_message_delete_single_product,
                    Toast.LENGTH_SHORT).show();
        }
    }
}