package com.example.android.inventoryapp.utilities;

/**
 * Created by Miki on 2018. 01. 29..
 */

public class Utilities {

    private static final String LOG_TAG = Utilities.class.getSimpleName();

    private Utilities() {
    }

    public static boolean isEmail(String input) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    public static boolean isPhone(String input) {
        return android.util.Patterns.PHONE.matcher(input).matches();
    }

    public static boolean isEmailOrPhone(String input) {
        return isPhone(input) || isEmail(input);
    }
}