package com.example.android.inventoryapp;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.inventoryapp.data.InventoryContract.ProductEntry;
import com.example.android.inventoryapp.utilities.Utilities;

import java.io.IOException;

public class EditorActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private static final int EXISTING_PRODUCT_LOADER = 28;
    private static final int REQUEST_IMAGE_GET = 123;
    private static final String PICTURE_EXTRA_KEY = "picture";

    // True if the editor add a new product to the database. False if it's edit an existing one.
    private boolean isNewProduct;

    // If the user changed any data, this is true
    private boolean isDataChanged = false;

    // This listener detects if a touch event occurred on any of the input fields.
    // If so, it changes the isDataChanged variable to true.
    private View.OnTouchListener editListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            isDataChanged = true;
            return false;
        }
    };

    private long productId = -1;

    // input fields
    ImageView pictureInput;
    EditText nameInput;
    EditText quantityInput;
    EditText priceInput;

    EditText supplierInput;
    Uri pictureUri = Uri.EMPTY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        // Determine if the activity edit an existing product, or create a new one
        if (getIntent().hasExtra(getString(R.string.intent_extra_key_product_id))) {
            setTitle(getString(R.string.editor_activity_edit_product_title));
            isNewProduct = false;
            productId = getIntent().
                    getLongExtra(getString(R.string.intent_extra_key_product_id), -1);
            getLoaderManager().initLoader(EXISTING_PRODUCT_LOADER, null, this);
        } else {
            setTitle(getString(R.string.editor_activity_new_product_title));
            isNewProduct = true;
        }

        pictureInput = findViewById(R.id.iv_select_image);
        nameInput = findViewById(R.id.et_name_input);
        quantityInput = findViewById(R.id.et_quantity_input);
        priceInput = findViewById(R.id.et_price_input);
        supplierInput = findViewById(R.id.et_supplier_input);

        // Set the editListener on the input fields, so we'll able to know if the user changes
        // anything.
        pictureInput.setOnTouchListener(editListener);
        nameInput.setOnTouchListener(editListener);
        quantityInput.setOnTouchListener(editListener);
        priceInput.setOnTouchListener(editListener);
        supplierInput.setOnTouchListener(editListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        pictureUri = Uri.parse(savedInstanceState.getString(PICTURE_EXTRA_KEY));
        try {
            pictureInput.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), pictureUri));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Cannot reload the image in onRestoreInstanceState");
        }
        super.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PICTURE_EXTRA_KEY, pictureUri.toString());
        super.onSaveInstanceState(outState);
    }

    /**
     * This method get called, when the user clicks on the image.
     * It's request an image from the phone (camera or file system)
     */
    public void selectImage(View view) {
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.intent_chooser_picture_title)),
                    REQUEST_IMAGE_GET);
        }
    }

    /**
     * When the activity get back the selected image as a result, this method is called.
     *
     * @param requestCode of the answer
     * @param resultCode  is the request was successful?
     * @param data        the answer to the request
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK && data != null) {
            try {
                pictureUri = data.getData();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    getContentResolver().takePersistableUriPermission(pictureUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                Bitmap pictureBitmap =
                        MediaStore.Images.Media.getBitmap(getContentResolver(), pictureUri);
                pictureInput.setImageBitmap(pictureBitmap);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.error_message_cannot_load_image),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_product_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save:
                saveProduct();
                return true;
            case android.R.id.home:
                if (isDataChanged) {
                    showUnsavedChangesAlert();
                } else {
                    setResultAndFinish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveProduct() {
        // Get the values of the editor
        String pictureUri = this.pictureUri.toString();
        String name = nameInput.getText().toString().trim();
        Integer quantity;
        Integer price;
        try {
            quantity = Integer.parseInt(quantityInput.getText().toString().trim());
            price = Integer.parseInt(priceInput.getText().toString().trim());
        } catch (NumberFormatException e) {
            Log.e(LOG_TAG, "Wrong input in the number input fields");
            Toast.makeText(this, getString(R.string.error_message_wrong_number),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        String supplier = supplierInput.getText().toString().trim();

        // Check if there is an empty or wrong input field
        if (name.isEmpty()) {
            Toast.makeText(this, getString(R.string.error_message_empty_text_input),
                    Toast.LENGTH_SHORT).show();
            return;
        } else if ((quantity != null && quantity < 0) || price == null || price <= 0) {
            Toast.makeText(this, getString(R.string.error_message_wrong_number),
                    Toast.LENGTH_SHORT).show();
            return;
        } else if (pictureUri.isEmpty()) {
            Toast.makeText(this, getString(R.string.error_message_wrong_image_input),
                    Toast.LENGTH_SHORT).show();
            return;
        } else if (supplier.isEmpty() || !Utilities.isEmailOrPhone(supplier)) {
            Toast.makeText(this, getString(R.string.error_message_not_valid_supplier),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        ContentValues values = new ContentValues();
        values.put(ProductEntry.COLUMN_NAME, name);
        values.put(ProductEntry.COLUMN_PICTURE, pictureUri);
        values.put(ProductEntry.COLUMN_QUANTITY, quantity);
        values.put(ProductEntry.COLUMN_PRICE, price);
        values.put(ProductEntry.COLUMN_SUPPLIER, supplier);

        if (isNewProduct) {
            Uri newProduct = getContentResolver().insert(ProductEntry.CONTENT_URI, values);
            if (newProduct == null) {
                Toast.makeText(this, getString(R.string.error_message_error_during_save),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.message_save_success),
                        Toast.LENGTH_SHORT).show();
                setResultAndFinish();
            }
        } else {
            int success = getContentResolver().update(
                    ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId),
                    values,
                    null,
                    null);
            if (success == 0) {
                Toast.makeText(this, getString(R.string.error_message_error_during_save),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.message_save_success),
                        Toast.LENGTH_SHORT).show();
                setResultAndFinish();
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (isDataChanged) {
            showUnsavedChangesAlert();
        } else {
            super.onBackPressed();
        }
    }

    private void showUnsavedChangesAlert() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        setResultAndFinish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_alert_message);
        builder.setPositiveButton(R.string.unsaved_alert_discard_button, listener);
        builder.setNegativeButton(R.string.unsaved_alert_keep_editing_button, listener);
        builder.show();
    }

    @Override
    public CursorLoader onCreateLoader(int id, Bundle args) {
        if (productId == -1) {
            return null;
        }
        String[] projection = new String[]{
                ProductEntry._ID,
                ProductEntry.COLUMN_NAME,
                ProductEntry.COLUMN_PICTURE,
                ProductEntry.COLUMN_QUANTITY,
                ProductEntry.COLUMN_PRICE,
                ProductEntry.COLUMN_SUPPLIER
        };
        return new CursorLoader(this,
                ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId),
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        cursor.moveToFirst();
        String name = cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_NAME));
        pictureUri =
                Uri.parse(cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_PICTURE)));
        Bitmap picture = null;
        try {
            picture = MediaStore.Images.Media.getBitmap(getContentResolver(), pictureUri);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.error_message_cannot_load_image),
                    Toast.LENGTH_SHORT).show();
        }
        int quantity = cursor.getInt(cursor.getColumnIndex(ProductEntry.COLUMN_QUANTITY));
        int price = cursor.getInt(cursor.getColumnIndex(ProductEntry.COLUMN_PRICE));
        String supplier = cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_SUPPLIER));

        // Set the values on the edit fields
        if (picture != null) {
            pictureInput.setImageBitmap(picture);
        }
        nameInput.setText(name);
        quantityInput.setText(String.valueOf(quantity));
        priceInput.setText(String.valueOf(price));
        supplierInput.setText(supplier);
    }

    @Override
    public void onLoaderReset(Loader loader) {
        // If the loader is invalidated, empty the input fields
        nameInput.setText("");
        pictureInput.setImageResource(R.drawable.no_image_startup);
        quantityInput.setText("");
        priceInput.setText("");
        supplierInput.setText("");
    }

    private void setResultAndFinish() {
        Intent data = new Intent();
        data.putExtra(getString(R.string.intent_extra_key_product_id), productId);
        data.putExtra(getString(R.string.intent_extra_key_product_name), nameInput.getText());
        setResult(RESULT_OK, data);
        finish();
    }
}
