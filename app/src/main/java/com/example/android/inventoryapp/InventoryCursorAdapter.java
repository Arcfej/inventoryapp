package com.example.android.inventoryapp;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.android.inventoryapp.customViews.SquareImageView;
import com.example.android.inventoryapp.data.InventoryContract.ProductEntry;

/**
 * This adapter display a list of products for the MainActivity
 */

class InventoryCursorAdapter extends CursorAdapter {
    private static final String LOG_TAG = InventoryCursorAdapter.class.getSimpleName();

    public interface OnSalesButtonClickListener {

        void onButtonClick(long id);
    }

    public interface OnProductClickListener {

        void onProductClick(long id, String productName);
    }


    private final OnSalesButtonClickListener buttonClickListener;
    private final OnProductClickListener itemClickListener;

    public InventoryCursorAdapter(Context context, Cursor cursor,
                                  OnSalesButtonClickListener salesButtonClickListener,
                                  OnProductClickListener listItemClickListener) {
        super(context, cursor, 0);
        this.buttonClickListener = salesButtonClickListener;
        this.itemClickListener = listItemClickListener;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.product_list_item, parent, false);
//        return new ViewHolder(context,
//                LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.product_list_item, parent, false));
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        SquareImageView thumbnailView = view.findViewById(R.id.iv_thumbnail);
        TextView nameView = view.findViewById(R.id.tv_product_name);
        TextView stockpileView = view.findViewById(R.id.tv_stockpile);
        TextView prizeView = view.findViewById(R.id.tv_product_price);
        Button saleButton = view.findViewById(R.id.b_sale);

        // Get the list item's values
        final int id = cursor.getInt(cursor.getColumnIndex(ProductEntry._ID));
        Uri pictureUri = Uri.parse(cursor.getString(
                cursor.getColumnIndex(ProductEntry.COLUMN_PICTURE)));
        Bitmap thumbnail = getThumbnail(context.getContentResolver(), pictureUri);
        final String productName = cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_NAME));
        int quantity = cursor.getInt(cursor.getColumnIndex(ProductEntry.COLUMN_QUANTITY));
        int price = cursor.getInt(cursor.getColumnIndex(ProductEntry.COLUMN_PRICE));

        // Display the values
        if (thumbnail != null) {
            thumbnailView.setImageBitmap(thumbnail);
        } else {
            thumbnailView.setImageResource(R.drawable.no_image_startup);
        }
        nameView.setText(productName);
        if (quantity > 0) {
            stockpileView.setText(context.getResources()
                    .getString(R.string.on_stock_quantity_plural, quantity));
        } else if (quantity == 1) {
            stockpileView.setText(context.getResources()
                    .getString(R.string.on_stock_quantity_single, quantity));
        } else {
            stockpileView.setText(context.getResources().getString(R.string.out_of_stock_quantity));
        }
        prizeView.setText(context.getResources().getString(R.string.product_price, price));
        // Set onclickListeners
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onProductClick(id, productName);
            }
        });
        saleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClickListener.onButtonClick(id);
            }
        });
    }

    /**
     * Sample is gotten from here:
     * https://stackoverflow.com/a/15396144
     *
     * @param cr            The ContentResolver
     * @param fullSizeImage uri of the full-size image
     * @return The id of the thumbnail of the full-size picture, or -1
     */
    public static Bitmap getThumbnail(ContentResolver cr, Uri fullSizeImage) {
        String wholeId = fullSizeImage.getLastPathSegment();
        long thumbId = Long.parseLong(wholeId.split(":")[1]);
        return MediaStore.Images.Thumbnails.getThumbnail(cr, thumbId,
                MediaStore.Images.Thumbnails.MICRO_KIND, null);
    }
}