package com.example.android.inventoryapp;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.inventoryapp.data.InventoryContract.ProductEntry;
import com.example.android.inventoryapp.utilities.Utilities;

import java.io.IOException;

public class DetailsActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final int PRODUCT_LOADER_ID = 33;

    public static final int GET_PRODUCT_ID_REQUEST = 600;
    private static final String PRODUCT_ID_INSTANCE_KEY = "product_id";

    private long productId;
    private Cursor cursor;
    private String contact;
    private String productName;

    private TextView quantityView;
    private ImageView picture;
    private TextView name;
    private TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent starterIntent = getIntent();
        productId = starterIntent.getLongExtra(getString(R.string.intent_extra_key_product_id), -1);
        setTitle(starterIntent.getStringExtra(getString(R.string.intent_extra_key_product_name)));

        quantityView = findViewById(R.id.tv_quantity_display);
        picture = findViewById(R.id.iv_image_details);
        name = findViewById(R.id.tv_name_details);
        price = findViewById(R.id.tv_price_display);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cursor == null || cursor.getCount() == 0 ||
                getLoaderManager().getLoader(PRODUCT_LOADER_ID) == null) {
            getLoaderManager().restartLoader(PRODUCT_LOADER_ID, null, this);
        } else {
            getLoaderManager().initLoader(PRODUCT_LOADER_ID, null, this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        getLoaderManager().destroyLoader(PRODUCT_LOADER_ID);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(PRODUCT_ID_INSTANCE_KEY, productId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        productId = savedInstanceState.getLong(PRODUCT_ID_INSTANCE_KEY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_PRODUCT_ID_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                productId = data.getLongExtra(getString(R.string.intent_extra_key_product_id), -1);
                setTitle(data.getStringExtra(getString(R.string.intent_extra_key_product_name)));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit_product) {
            Intent intent = new Intent(this, EditorActivity.class);
            intent.putExtra(getString(R.string.intent_extra_key_product_id), productId);
            startActivityForResult(intent, GET_PRODUCT_ID_REQUEST);
        } else if (id == R.id.action_delete_product) {
            showDeleteAlertDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void decreaseQuantity(View view) {
        int quantity = Integer.parseInt(quantityView.getText().toString()) - 1;
        if (quantity <= 0) {
            return;
        }
        ContentValues newQuantity = new ContentValues();
        newQuantity.put(ProductEntry.COLUMN_QUANTITY, quantity);
        int success = getContentResolver().update(
                ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId),
                newQuantity,
                null,
                null);
        if (success > 0) {
            quantityView.setText(String.valueOf(quantity));
        }
    }

    public void increaseQuantity(View view) {
        int quantity = Integer.parseInt(quantityView.getText().toString()) + 1;
        if (quantity <= 0) {
            return;
        }
        ContentValues newQuantity = new ContentValues();
        newQuantity.put(ProductEntry.COLUMN_QUANTITY, quantity);
        int success = getContentResolver().update(
                ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId),
                newQuantity,
                null,
                null);
        if (success > 0) {
            quantityView.setText(String.valueOf(quantity));
        }
    }

    public void contactSupplier(View view) {
        if (Utilities.isEmail(contact)) {
            Intent writeEmail = new Intent(Intent.ACTION_SENDTO);
            writeEmail.setData(Uri.parse("mailto:"));
            writeEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{contact});
            writeEmail.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject, productName));
            if (writeEmail.resolveActivity(getPackageManager()) != null) {
                startActivity(Intent.createChooser(writeEmail,
                        getString(R.string.intent_chooser_email_title)));
            }
        } else if (Utilities.isPhone(contact)) {
            Intent openPhone = new Intent(Intent.ACTION_DIAL);
            openPhone.setData(Uri.parse("tel:" + contact));
            if (openPhone.resolveActivity(getPackageManager()) != null) {
                startActivity(openPhone);
            }
        } else {
            Toast.makeText(this, getString(R.string.error_message_invalid_contact),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (productId == -1) {
            return null;
        }
        String[] projection = new String[]{
                ProductEntry.COLUMN_NAME,
                ProductEntry.COLUMN_PICTURE,
                ProductEntry.COLUMN_QUANTITY,
                ProductEntry.COLUMN_PRICE,
                ProductEntry.COLUMN_SUPPLIER
        };
        return new CursorLoader(this,
                ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId),
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Set the views' content
        this.cursor = cursor;
        cursor.moveToFirst();
        Uri bitmapUri =
                Uri.parse(cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_PICTURE)));
        try {
            Bitmap pictureBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), bitmapUri);
            picture.setImageBitmap(pictureBitmap);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.error_message_cannot_load_image),
                    Toast.LENGTH_SHORT).show();
        }
        productName = cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_NAME));
        name.setText(productName);
        price.setText(getString(R.string.price_display,
                cursor.getInt(cursor.getColumnIndex(ProductEntry.COLUMN_PRICE))));
        quantityView.setText(String.valueOf(
                cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_QUANTITY))));
        contact = cursor.getString(cursor.getColumnIndex(ProductEntry.COLUMN_SUPPLIER));

        getLoaderManager().destroyLoader(PRODUCT_LOADER_ID);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        name.setText("");
        picture.setImageResource(R.drawable.no_image_startup);
        quantityView.setText("");
        price.setText("");
        cursor.close();
        getLoaderManager().destroyLoader(PRODUCT_LOADER_ID);
    }

    private void showDeleteAlertDialog() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        deleteProduct();
                        dialog.dismiss();
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.delete_single_dialog_message, productName));
        builder.setPositiveButton(R.string.delete_dialog_delete_putton, listener);
        builder.setNegativeButton(R.string.delete_dialog_cancel_button, listener);
        builder.show();
    }

    private void deleteProduct() {
        int success = getContentResolver().delete(
                ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId),
                null,
                null);
        if (success == 1) {
            Toast.makeText(this, R.string.delete_single_product_success_message,
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.error_message_delete_single_product,
                    Toast.LENGTH_SHORT).show();
        }
    }
}
