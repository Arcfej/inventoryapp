package com.example.android.inventoryapp.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.example.android.inventoryapp.data.InventoryContract.ProductEntry;
import com.example.android.inventoryapp.utilities.Utilities;

/**
 * The contentProvider of the app.
 */

public class InventoryProvider extends ContentProvider {

    public static final String LOG_TAG = InventoryProvider.class.getSimpleName();

    // URI matcher code for the the whole inventory (products table)
    public static final int INVENTORY = 100;

    // Uri matcher code for a single product
    public static final int PRODUCT_ID = 101;

    // The UriMatcher for the provider
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(InventoryContract.CONTENT_AUTHORITY,
                InventoryContract.PATH_PRODUCTS,
                INVENTORY);
        uriMatcher.addURI(InventoryContract.CONTENT_AUTHORITY,
                InventoryContract.PATH_PRODUCTS + "/#",
                PRODUCT_ID);
    }

    // The databaseOpenHelper of the provider
    private InventoryDbHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new InventoryDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor;

        int queryCode = uriMatcher.match(uri);
        switch (queryCode) {
            case INVENTORY:
                // Give back the rows with the given parameters
                cursor = db.query(
                        ProductEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case PRODUCT_ID:
                // Give back only one row. The row ID is in the given URI
                selection = ProductEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};

                cursor = db.query(
                        ProductEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new IllegalArgumentException("Wrong URI, couldn't query: " + uri);
        }

        // Set a notification URI on the cursor,
        // so it will be updated if it's content should be changed.
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int insertCode = uriMatcher.match(uri);
        switch (insertCode) {
            case INVENTORY:
                return insertProduct(uri, values);
            default:
                throw new IllegalArgumentException("Not supported insertion: " + uri);
        }
    }

    /**
     * Insert a new product into the inventory database and throw IllegalArgumentException if the
     * insertion not supported.
     *
     * @param uri    of the insertion
     * @param values is the ne product
     * @return the inserted row's ID appended to the given uri.
     */
    private Uri insertProduct(Uri uri, ContentValues values) {
        String name = values.getAsString(ProductEntry.COLUMN_NAME);
        if (TextUtils.isEmpty(name)) {
            throw new IllegalArgumentException("The product requires a name!");
        }

        String picture = values.getAsString(ProductEntry.COLUMN_PICTURE);
        if (picture == null) {
            throw new IllegalArgumentException("The product requires a picture!");
        }

        Integer quantity = values.getAsInteger(ProductEntry.COLUMN_QUANTITY);
        if (quantity != null && quantity < 0) {
            throw new IllegalArgumentException("You cannot have a negative number of product!");
        }

        Integer price = values.getAsInteger(ProductEntry.COLUMN_PRICE);
        if (price == null || price <= 0) {
            throw new IllegalArgumentException("You have to provide a valid price!");
        }

        String supplier = values.getAsString(ProductEntry.COLUMN_SUPPLIER);
        if (supplier == null || !Utilities.isEmailOrPhone(supplier)) {
            throw new IllegalArgumentException("You have to have a supplier for the product!");
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long insertedId = db.insert(ProductEntry.TABLE_NAME, null, values);
        if (insertedId == -1) {
            Log.e(LOG_TAG, "Failed to insert new row: " + uri);
            return null;
        }

        // Notify all the listeners, that the data changed on the given URI
        getContext().getContentResolver().notifyChange(uri, null);

        // Return the ContentUri appended with the inserted row's ID
        return ContentUris.withAppendedId(uri, insertedId);
    }


    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        int updateCode = uriMatcher.match(uri);
        switch (updateCode) {
            case INVENTORY:
                return updateProduct(uri, values, selection, selectionArgs);
            case PRODUCT_ID:
                selection = ProductEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updateProduct(uri, values, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Cannot update the database at " + uri);
        }
    }

    /**
     * Updates a product in the inventory database.
     *
     * @param uri           which points to the table or to a row of the table
     * @param values        is the values to update with
     * @param selection     is the rows to update
     * @param selectionArgs is the arguments of the selected rows (how to chose the rows?)
     * @return the number of the updated rows
     */
    private int updateProduct(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // Checks if the ContentValues contains any values to update with
        if (values.size() == 0) {
            return 0;
        }

        // Checks if the ContentValues contains a value for each column.
        // If it contains, check again if it's a valid value or not.
        if (values.containsKey(ProductEntry.COLUMN_NAME)) {
            String name = values.getAsString(ProductEntry.COLUMN_NAME);
            if (name == null) {
                throw new IllegalArgumentException("The product requires a name!");
            }
        }

        if (values.containsKey(ProductEntry.COLUMN_PICTURE)) {
            String picture = values.getAsString(ProductEntry.COLUMN_PICTURE);
            if (picture == null) {
                throw new IllegalArgumentException("The product requires a picture!");
            }
        }

        if (values.containsKey(ProductEntry.COLUMN_QUANTITY)) {
            Integer quantity = values.getAsInteger(ProductEntry.COLUMN_QUANTITY);
            if (quantity != null && quantity < 0) {
                throw new IllegalArgumentException("You cannot have a negative number of product!");
            }
        }

        if (values.containsKey(ProductEntry.COLUMN_PRICE)) {
            Integer price = values.getAsInteger(ProductEntry.COLUMN_PRICE);
            if (price == null || price <= 0) {
                throw new IllegalArgumentException("You have to provide a valid price!");
            }
        }

        if (values.containsKey(ProductEntry.COLUMN_SUPPLIER)) {
            String supplier = values.getAsString(ProductEntry.COLUMN_SUPPLIER);
            if (supplier == null || !Utilities.isEmailOrPhone(supplier)) {
                throw new IllegalArgumentException("You have to have a supplier for the product!");
            }
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int updatedRowsCount = db.update(ProductEntry.TABLE_NAME, values, selection, selectionArgs);

        // If at least on row row's updated, notify the listeners about it.
        if (updatedRowsCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return updatedRowsCount;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int deletedRowsCount = 0;

        int deleteCode = uriMatcher.match(uri);
        switch (deleteCode) {
            case INVENTORY:
                // Delete all the code at selection and selectionArgs
                deletedRowsCount = db.delete(ProductEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case PRODUCT_ID:
                // Delete a single row. The row's ID is in the uri
                selection = ProductEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                deletedRowsCount = db.delete(ProductEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Not supported delete operation for " + uri);
        }

        // If a deletion is happened, notify the listeners about it
        if (deletedRowsCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deletedRowsCount;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int uriCode = uriMatcher.match(uri);
        switch (uriCode) {
            case INVENTORY:
                return ProductEntry.CONTENT_LIST_TYPE;
            case PRODUCT_ID:
                return ProductEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("No match for the URI: " + uri);
        }
    }
}
